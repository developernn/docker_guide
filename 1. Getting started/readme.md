### What is docker?
It is a service for running apps inside containers.

We can use docker when as in development as in production (usually in production used kubernetes, because it automates start of containers in different servers).


### History of docker
May be u heard about virtual mashines. 
They have disadvatages because iso of vm take a lot of resources. Also they are big, so u cant just take them and transfer to anorther mashine and развернуть.


### Difference between VM and containers

| VM        | Containers        |
| ------------- |:-------------:|
| Подразумевает виртуализацию железа для запуска гостевой ОС      | Используют ядро хостовой машины |
| col 2 is      | centered      |
| Идеальна для изоляции | Плохо для изоляции     |

1.png


### Philosophy of docker
For one process we create one container. 
All dependency shoud be inside a container.
Then smaller container then better. 
Stadart of packaging apps
Garante of воспроизводимость
Each container is a isolated enviroment.
All contners from the same image behave the same.


### Advantages of using docker
1. Apps is running in isolated enviroment 
App which is running inside container is isolated from anorther containers and host computer (on which docker is running).

2. It is easy to start apps in different enviroments
U just need take образ and start container.

3. All dependency inside a container.

4. It is easy to масштабировать
U just need to start new containers

5. It is convinient to use for development 
All dependency inside a container. 
Nothing need to be installed. 
We just need image from dockerhub and deploy container.


12 факторов разработки
1. Единный репозиторий исходного кода
Мы используем систему контроля версий и разварачиваем приложения для разных сред из одного репозитория
2. Зависимости
Нужно явно объявлять и изолировать зависимости. Все зависимости должны быть в манифесте зависимостей.Современные языки предоставляют manager пакетов с манифестом зависимостей. Кроме того изолированные должны быть чтобы системная библиотека не попала в зависимости.
3. Кофигурация
Параметры которые меняются в зависимости от того где запущен преокт.
Это свойство среды выполнения. Код не зависит от окружения а конфигурация зависит.
Поэтому код храниться в репозитории а конфигурация в окржение.
Популярный вариант использованние заранее подготовленных конфигураций например development и production.
4. Сторонние службы (backing services)
Например, базы данных. Они должны быть подключаемым ресурсом. Данные для подключения храняться в конфигурации.
5. Сборка, релиз, выполнение
Развертование приложение состоит из трех независимых этапов. 
Сборка = преобразование кода в исполняемый пакет
Релиз = объединение сборки с конфигурацией 
Выполнение (runtime) – запуск некоторого количества процессов из релиза.
Нужно строго разделять эти этапы.
6. Изолированные процессы
Запускать приложение как одно или несколько процессов, не сохраняющих внутреннее состояние. Приложение может использовать память компьютера, но данные пользователя должны храниться в постоянном месте хранилища, которое будет подключаемым приложением. 
7. Привязка портов
Приложение должно быть полностью самодостаточное и не полагаться на что либо. 
8. Параллелизм
Мастштабируйте приложение с помощью процессов. Правильно разработанное приложение поддерживает горизонтальное масштабированние просто добавлением аналогичного экземпляра системы. 
9. Утилизируемость
Компоненты системы должны быть построены таким образом что они могут быть запущены, остановлены или уничтожены в любой момент.
Важным фактом тут добавление тестов проверки правильности запуска приложений и проверки доступности всех необходимых систем для работы приложения
10. Соотвествие среды разработки / работы приложения
Stage и production максимально похожи. 
11. Журналирование
Важный компонент логи. Возможность перенаправление в систему агрегации логов, такую как efk или greylock. 
12. Задачи администрирования
Код задач должен лежать в репозиториях. Например, миграции. 




? кстати интересно попробоывать собрать свой контейнер, положить его в gitlab и чтобы другая репа использовала его


### Why we need this?
- Before container
for start development we need to setup a lot of dependencies and make a lot of steps to setup our enviroment. Something may go wrong. 
More app is complex more it is hard to set up all this. And u need repeat it for all developers enviroment. 

- After container
 U dont install any dependency directly to your OS because the container it is own isolated OS layer with linux based image. 

U have eveything packages in one isolated enviroment. So we have everything: programm, configuration and start script inside one container.

So as developer u dont have to find binaries, rather u check out the container repository to find container and download to your local mashine. Download is just one docker command. It is fetch container and start it.

В общем раньше было приложение, его надо было собирать, конфигурировать, депенденси все скачивать. Все эти депенденси тоже надо было конфигурировать и так далее. 
С контейнрами же 
Теперь же мы типо разрабатываем все в конейтенер, и в нем уже есть все депенденси и конфигурация. И просто закидываем этот контейнер на сервер, также все депенденси в контейнерах, просто по факту устанавливаем и все работает.














