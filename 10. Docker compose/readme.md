### Docker compose
It is used to automate start of container. Pretty useful when we have a lot.

Dockerfile is used to describe instructions for container. But also we have docker compose file which allow run and stop several containers with one command.
So, it allows to give instructions how to create several containers.

кстати про файлы не скзаал расширения

Example of docker-compose.yml file:
```
version: '3'
services:
    app: 
        build: ./app
    mongo: 
        image: mongo
```
app, mongo - name of our services. For each service/app/process we want create own container. 
build <path for files>
image <name of image>



It is very convinient:
* run all container with one command 
- automatic creation необходимых images based on dockerfile of each app
- automatic creationg isolate network for взаимодействия containers
Я так понимаю они в одной сети
- Using dns communication between containers can be sued using names
"mongodb::/mongo:27017" We use mongo name of our service instead of ip address.


Usually apps consists of several process/apps. So we need several containers. 

Обычно микросервисное приложение это несколько сервисов которые представляют собой несколько контейнеров которые требуют согласованности в запуске и настройки какого-то взаимодействия. Чтобы решить это проблему используют docker compose. 

То есть это инструмент для описания и запуска приложений, которые состоят из нескольких контейнеров. 

Allow to start and setup many-contaner app

All container describes in docker-compose.yml. 
Container called services. 

Create own network for project. 

Allow services to communicate by names

То есть мы описываем в yml file набор наших приложений а затем с помощью docker-compose  запустить файл в нужном порядке. 

Весь наш набор приложений в терминологии докер это проект. Compose использует имя преокта для изоляции, поэтому мы можем запускать на одном хосте несколько одинаковых проектов с разными именами. Имя проекта по-умолчанию такое же где находиться файл yml.
Каждое приложение в проекте называется сервисом.


### YAML
Docker compose use yaml file for creating instructions. 
This format is convieneit for configuration, because we have keys and values. 

### YAML syntax



### Workflow
1. Write code 
2. Dockerfile. Describe all dependency and how to build image
3. Docker compose. Describe how to start services/containers


# Docker compose commands
* Run docker compose 
```
docker-compose up
// stop all container and delete them.
// also network
docker-compose down
```

* Change something
```
docker-compose up -d --build 
```
-d = detach mode 
--build = заново создать все необходимые образы переписав старые




Ну в общем сервисы могут взаимодействовать между собой используя имена сервисов если они в одной сети.
Интересно посмотреть как это работает. Где файл днс.






















