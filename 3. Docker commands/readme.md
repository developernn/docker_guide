# Commands
<something> - required placeholder
<<something>> - optional placeholder

Чтобы разбить команду на строки: \

Container has access to internet. 


### Structure of commands
utilite + category of manage + command 
docker container ls

Команда которая должна сделать что-то с определенным образом или контейнером требует в качестве аргумента id или имя образа или контейнера
утилита командной строки; категория управления; команда; аргумент (id/name)
docker container rm my-container 

Also there is new syntax.
8.png 


### Commands 
1. Commands for working with containers 
* Create container
docker container create my_repo/my_image:my_tag
my_repo - docker registery 
my_image - name of image 
my_tag - tag of image 

Кстати да не только output но и пояснение команд. 

By default docker use docker hub as repository. 
By default uses tag "latest". For production don't use it. 

* Start container 
We can start containers wich was stoped. 
Docker ps -a
Then:
docker container start my_container
docker start id – to start stoped container 


* Restart container 
docker container restart
docker restart id

* Run container 
Download image from registery (if it doesn't exists locally), create container from image and start it
docker run my_image 
pulls image and run a container 

docker container run -it --rm -p 8080:80 my_image 
-i - interactive, means stding will be opened 
-t create pseude linux shell (tty), which will be connected to stdin and stdout. 
--rm dekete containr when it finished 
-p - port. First is port of docker. 
-d (--detach) - start container in background mode. 

* List of running containers 
docker container ls 
docker ps 

Options: 
-a - all containers (not only running)
-s - size of each container 

* Useful info about container 
docker container inspect my_container 

* Logs of container 
docker container logs my_container
docker container logs my-container -f = interactive mode of logs (what is happening now)

* Stop container 
By default give 10 seconds for stop processes. 
docker container stop my_container 
docker stop id

Принудительное
docker container kill my_container 

Kill all containers 
docker container kill $(docker ps -q)

* Delete container 
docker container rm my_container - delete stoped container. U can't delete running container, firstly u need stop it. 

docker container rm $(docker ps -a -q) - delete all containers


2. Commands for working with images
* Create image 
docker image build -t my_repo:my_image:my_tag . 
-t give tag 
my_report - name of rep, by default docker hub.

Make point on "." at the end. 

* Publish builded image in registry
docker image push my_repo:my_image:my_tag
Before push u need log in in registery. 

* Log in in registery 
docker login 

* Searrch in registery
docker search 

* Get image from registery 
docker pull 

* List of images
docker image ls 

* Check history of промежуточных images 
docker image history my_image 

* Info about image 
For example layers
docker image inspect my_image 

* Delete image
docker image rm my_image 
docker rmi my_image 

Delete all images on host 
docker image rm $(docker images -a -q)



3. Different
* Info about client and server
docker version 

* Remove all not running containers, networks, containers witout images
docker system prune 
Delete all not used images, don't used toms:
docker system prune -a --volumes








### The main commands
* Informatation about client and server
```
docker version
```

* List of containers
running and stoped:
```
docker ps -a
```

```

```


* List of local images (from docker cache)
```
docker images
```

* Create and run container from image
Сначала докер ищет локально образ, если его нет идет на докер хаб. Если там такой образ найден – он созраняет его локально и после этого запускает контейнер.
То есть docker run запускает docker pull если образ локально не найден?

```
docker run -it busybox 
```

Флаги:
-i = интерактивный 
-t = терминал 
Можно совмещать -it 
-d = Запуск контейнера в background в фонов режиме 
--name <name> = name for container 
Для того чтобы открыть доступ к какому-то сервису внутри контейнера, который работает на определенном порту вам необхдимо:
-p <внешний порт>:<порт контейнера>
публикация порта или открытие порта на нашем компьютере и пробросить его на другом порт внутри конетйнера. Эту проброску делает docker.
Docker run -p 8080:80 nginx 
Запуститься контейнер к которому мы сможем обращаться на порту 8080.
Можно открывать несколько портов. Например mongodb требует несколько портов.

А порт я так понимаю на котором работает сервис можно узнать из docker container inspect?

Кстати а проброска портов разве безопасно?

Mapping томов
-v <local path>:<container path>
For example:
docker run -v ${PWD}:/usr/share/ nginx 
То есть мы тут указываем путь к локальной папке и к папке внутри контейнера.

V = volume = tome 

То есть с помощью этой команды мы заменять папку которую мы указали после : в контейнере на папку которую мы указали до : на нашем локальном компьютере.

Бл я чет забыл а телка также делала сохранения перманентое данных из базы данных? Если да че за папку надо меппить. Че то я забыл.

Docker ps меппинга томов нет, нужно inspect юзать.
“Mounts”

--rm = автоматически удалить контейнер когда он остановится.


Можете выполнить команду 
docker run hello-world 
и там кучу полезной инфы напишет.



* Save image locally
docker hub можно найти любой образ с описанием работы с ним.
```
Docker pull <image>
```


* Delete container
```
Docker rm <id/name>
```

* Stop container
```
docker stop <id/name>
```

* Remove all stopped containers 
```
docker container prune
```

docker sheets надо погуглить
Еще нужно output для всех команд показать и обхяснить его


* Info about container
```
docker container inspect <id/name>
```
For example to find ip of container.


* Start extra process inside running container
```
docker exec <id/name> <process name>
```
Например: docker exec -it mycontainer bash
-it = интерактивный терминал

Надо еще не только команду писать но и пример

* 
```

```

* 
```

```

* 
```

```

















