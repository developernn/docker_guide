# Creating images 

Earlier we use docker hub for getting images. But we can also create our own images and later use them to run containers. 

To run app/process in a container we need firstly create image and записать в него:
1. Files of app
2. Add instructions how to run app inside a container, which will be create based created image 

Created images cuold be downloaded to docker hub.


### Этапы creation of image
1. Create dockefile 
It is a special file with instructions for docker for creating new image 
one docker file = 1 image 
This file is stored inside root directory of project 
When creating image we can specify name and tag for creating image 
2. Create image based on docker file 
3. Create container based on image 


### 1. Create dockerfile

What is a dockerfile?
It is a file which describe how to build image. 

Example:
```

// [name of base image]:[tag(version) of image]
// Our base image is python. 
FROM python:alpine 

// Working directory inside image. It is recomded to create directory for your app inside image (prevent overwrite of system dirs). Automatically after this command happen, cd to this directopry happen.
WORKDIR /app

// copy files from current directory to workdir
COPY . .

// Commands which will be executed when new container is created. We start pythom script. 
// We need to start some process inside a container
CMD ["python", "main.py"]

```

### 2. Creating image from dockerfile
After dockerfile is ready, it means we describe all instructions. Now we can build an image:
* Create an image
```
docker build <path to docker file folder>
```
-f <docker file name> = если файл называется не docker
-t <image name>:<tag> = добавление имени и тэга для образа
Если отпустить тег то автомотически будет добавлен тег latest.
Если мы два раза выполнем команду, то есть два раза повториться тег и имя – то перезапишется образ.

After building image we can explore it and run container:
```
docker images
docker run <id>
```

### Creating new version
For example u change someting in app and wuold like to create new image. 
```
docker build . -t name:tag
```


Наверное нужно пересмотреть видосы и спиздеть как там создавали эти image. 


### Docker file commands
RUN <command> - run command inside a container
















