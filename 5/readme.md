Также есть еще абстракции например docker file – это абстракция для создания контейнера. В ней каждая команда является новым слоем. 



### How container works?
Контейнеры создаются на базе ОС Linux, которая имеет linux kernel, ram, cpu, network (сетевые ресурсы). Также есть disk. 
Для того чтобы создавать контейнеры нужен docker engine, который запускает docker deamon (сервис докер).
Это минимальный набор для создания контейнеров. 


### What is happening when we create container?
- After container is created on hard drive some files created. They will be accesable only for this container. It is isolated space where containers files are saved. 
- After this container start certain process. 
Заметьте так как ядро общее, то оно будет разделяемое между всеми контейнерами. Процесс контейнера изолирован от других процессов.

3.png

Важно понимать так как ресурсы разделяемые – то вы должн грамотно использовать контейнеры.

Также важно учитывать: если мы запустили несколько контейнеров из одного образа, то у этих контейнеров будут общие файлы, которые будут находиться в одном месте на жестком диске docker хоста. Короче не будет копий.


### What is happening after we stop and delete container?
All files created by container is deleted. 
Also doccker automatically stops containers witout running processes. 

4. png


### How docker run on different os?
5. png

Мы сказали что docker работает под linux. Как запустить докер не на линукс?
Docker desktop – создает виртуальную машину под управлением linux внутри которой запустем docker deamon (docker сервис). Все контейнеры будут создаваться внутри этой ВМ.
Ну а в продакшене обычно сервера с linux, и на них устанавливается docker engine. Нет необходимости в ВМ.


надо кстати из видосов прошлых практику спиздить, а то так мало понятно. 






